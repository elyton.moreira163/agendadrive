<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::middleware(['auth'])->group(function () {

    /* pagina principal
    Route::get('/', function () {
        return view('index');
    });*/

    /* pagina sobre */
    Route::get('/sobre', function () {
        return view('sobre');
    });

    /* pagina principal de atividades*/
    Route::get('/atividades', 'ControladorAtividade@index');

    /* pagina principal de disciplinas*/
    Route::get('/disciplinas', 'ControladorDisciplina@index');

    /* manda pra pagina de adicionar disciplina*/
    Route::get('/disciplinas/novadisciplina', 'ControladorDisciplina@create');

    /* salva disciplina no banco de dados*/
    Route::post('/disciplinas', 'ControladorDisciplina@store');

    /* lista as disciplinas e os tempos na home*/
    Route::get('/', 'ControladorDisciplina@ordenarDisciplinas');

    /* apagar disciplina */
    Route::get('/disciplinas/apagar/{id}', 'ControladorDisciplina@destroy');

    /* abrir pagina editar disciplina */
    Route::get('/disciplinas/editar/{id}', 'ControladorDisciplina@edit');

    /* atualizar disciplina */ 
    Route::post('/disciplinas/{id}', 'ControladorDisciplina@update');

    /* manda pra pagina de adicionar atividade*/
    Route::get('/atividades/novaatividade', 'ControladorAtividade@create');

    /* salva atividade no banco de dados*/
    Route::post('/atividades', 'ControladorAtividade@store');

    /* apagar atividade */
    Route::get('/atividades/apagar/{id}', 'ControladorAtividade@destroy');


    /* abrir pagina editar atividade */
    Route::get('/atividades/editar/{id}', 'ControladorAtividade@edit');

    /* atualizar atividade */ 
    Route::post('/atividades/{id}', 'ControladorAtividade@update');



    /* restaurar atividade */
    Route::get('/atividades/restaurar/{id}', 'ControladorAtividade@restore');


    /* restaurar atividade */
    Route::get('/atividades/apagarpermanente/{id}', 'ControladorAtividade@apagarpermanente');



    /* ir para view de disciplinas apagadas */
    Route::get('/restaurardisciplina', 'ControladorDisciplina@disciplina_apagada');


    /* restaurar atividade */
    Route::get('/disciplinas/restaurar/{id}', 'ControladorDisciplina@restore');

    /* lista de atividades concluidas */
    Route::get('/atividades/atividadesconcluidas', 'ControladorAtividade@atividade_concluida');

});























Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
