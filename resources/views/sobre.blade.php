@extends('layout.app', ["current" => "sobre"])

@section('body')
 
<h4>Breve Descrição da aplicação</h4>

<h5>     O site foi desenvolvido com o intuito de auxiliar os acadêmicos do IFRR com o gerenciamento de seu calendário escolar e de suas atividades.<br><br>
    Esta foi uma necessidade dos próprios desenvolvedores que, em certos momentos, também tinham dificuldades para organizar seus compromissos e sentiam falta de uma ferramenta para este fim.<br><br> O sistema foi elaborado e desenvolvido para a disciplina de TCC do Curso de Tecnologia em Análise e Desenvolvimento de Sistemas do Instituto Federal de Educação, Ciência e Tecnologia de Roraima (IFRR).


<br>
<br>
<br>
<h4>Nome dos desenvolvedores</h4>

<h5>Dayanne Pereira Moura</h5>
<h5>Keytiane Aurea da Cunha Amorim</h5>
<h5>Pedro Paulo Level Salomão Alves</h5>

        <a href="/" class="btn btn-lg btn-primary" role="button" style="margin-top: 30px">Voltar a Tela inicial</a>


@endsection