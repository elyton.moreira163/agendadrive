@extends('layout.app', ["current" => "atividades"])

@section('body')

<!--
 <div class="card border">
    <div class="card-body">
        <h3 class="card-title">Atividades em Andamento</h3>

 @if(count($atividade) > 0)
        <table class="table table-ordered table-hover table-responsive-xl">
            <thead class="thead-dark">
                <tr>
                    <th>Tipo</th>
                    <th>Data de Entrega</th>
                    <th>Disciplina</th>
                    <th>Descrição</th>
                    
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
   @foreach($atividade as $ati)
                <tr>
                    <td>{{$ati->tipo}}</td>
                    <td>{{$ati->dataentrega}}</td>
                    <td>{{$ati->nomedisciplina}}</td>
                    <td>{{$ati->descricao}}</td>
                    <td>
                        <a href="/atividades/editar/{{$ati->id}}" class="btn btn-sm btn-primary">Editar</a>
                        <a href="/atividades/apagar/{{$ati->id}}" class="btn btn-sm btn-success">Concluida</a>
                        <a href="/atividades/apagarpermanente/{{$ati->id}}" class="btn btn-sm btn-danger">Apagar</a>
                        
                    </td>
                </tr>
    @endforeach                
            </tbody>
        </table>
@else
        <h5 style="margin-top: 30px" >Você não possue atividades pendentes</h5>

@endif 


    </div>
    <div class="card-footer">
        <a href="/atividades/novaatividade" class="btn btn-lg btn-primary" role="button">Adicionar Atividade</a>
    </div>
</div>


 <div class="card border" style="margin-top: 30px">
    <div class="card-body">
        <h3 class="card-title">Atividades Concluidas</h3>

 @if(count($atividadeConcluida) > 0)
        <table class="table table-ordered table-hover table-responsive-xl">
            <thead class="thead-dark">
                <tr>

                    <th>Tipo</th>
                    <th>Data de Entrega</th>
                    <th>Disciplina</th>
                    <th>Descrição</th>
                    
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
   @foreach($atividadeConcluida as $atiConcluida)
                <tr>
                    <td>{{$atiConcluida->tipo}}</td>
                    <td>{{$atiConcluida->dataentrega}}</td>
                    <td>{{$atiConcluida->nomedisciplina}}</td>
                    <td>{{$atiConcluida->descricao}}</td>
                    <td>
                        <a href="/atividades/restaurar/{{$atiConcluida->id}}" class="btn btn-sm btn-success">Restaurar</a>
                        <a href="/atividades/apagarpermanente/{{$atiConcluida->id}}" class="btn btn-sm btn-danger">Apagar</a>
                        
                    </td>
                </tr>
    @endforeach                
            </tbody>
        </table>
@else
        <h5 style="margin-top: 30px">Você não possue atividades Concluidas</h5>
@endif 
       

     </div>
</div>
-->
 <div class="card border">
    <div class="card-body">

 @if(count($atividade) > 0)
        <div class="list-group">
            <a class="list-group-item list-group-item-action flex-column align-items-start list-group-item-primary">
                <div class="d-flex w-100 justify-content-between">
                    <h4 class="mb-1">Atividades em Andamento</h4>
                </div>
            </a>
        </div>
    @foreach($atividade as $ati)

        <div class="list-group" style="margin-top: 30px">
            <a class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Tipo de Atividade: {{$ati->tipo}}</h5>
                    <h5 class="mb-2">Data de entrega: {{$ati->dataentrega->format('d/m/Y')}}</h5>
                </div>
                <div class="d-flex w-100 justify-content-between">
                   @foreach($disciplina as $dis)
                        @if($ati->disciplina_id === $dis->id)
                            <h6 class="mb-1">Disciplina: {{$dis->nome_disciplina}}</h6>
                        @endif
                    @endforeach
                </div>
                <p class="mb-1">Descrição: {{$ati->descricao}}</p>
                <small>
                    <a href="/atividades/editar/{{$ati->id}}" class="btn btn-md btn-primary" style="margin-right: 15px" >Editar</a>
                    <a href="/atividades/apagar/{{$ati->id}}" class="btn btn-md btn-success" style="margin-right: 15px">Concluida</a>
                    <a href="/atividades/apagarpermanente/{{$ati->id}}" class="btn btn-md btn-danger" style="margin-right: 15px">Apagar</a>   
                </small>

            </a>
        </div>

    @endforeach 

@else
        <div class="list-group" >
        <a class="list-group-item list-group-item-action flex-column align-items-start list-group-item-primary">
            <div class="d-flex w-100 justify-content-between">
                <h4 class="mb-1">Você não possui atividades em andamento</h4>
            </div>
            </a>
    </div>
@endif

     </div>
</div>

<!--
 <div class="card border" style="margin-top: 20px">
    <div class="card-body">

 @if(count($atividadeConcluida) > 0)
        <div class="list-group" >
            <a class="list-group-item list-group-item-action flex-column align-items-start list-group-item-success">
                <div class="d-flex w-100 justify-content-between">
                    <h4 class="mb-1">Atividades Concluidas</h4>
                </div>
            </a>
        </div>
    @foreach($atividadeConcluida as $atiConcluida)


        <div class="list-group" style="margin-top: 30px">
            <a class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Tipo de Atividade: {{$atiConcluida->tipo}}</h5>
                    <h5 class="mb-2">Data de entrega: {{$atiConcluida->dataentrega->format('d/m/y')}}</h5>
                </div>
                <div class="d-flex w-100 justify-content-between">
                    @foreach($disciplina as $dis)
                        @if($dis->id ===  $atiConcluida->disciplina_id)
                            <h6 class="mb-1">Disciplina: {{$dis->nome_disciplina}}</h6>
                        @endif
                    @endforeach

                </div>
                <p class="mb-1">Descrição: {{$atiConcluida->descricao}}</p>
                <small>
                        <a href="/atividades/restaurar/{{$atiConcluida->id}}" class="btn btn-md btn-success" style="margin-right: 15px">Restaurar</a>
                        <a href="/atividades/apagarpermanente/{{$atiConcluida->id}}" class="btn btn-md btn-danger" style="margin-right: 15px">Apagar</a> 
                </small>

            </a>
        </div>
    @endforeach 

@else
        <div class="list-group" >
            <a class="list-group-item list-group-item-action flex-column align-items-start list-group-item-success">
                <div class="d-flex w-100 justify-content-between">
                    <h4 class="mb-1">Você não possui atividades concluidas</h4>
                </div>
            </a>
        </div>
@endif

     </div>
</div>
-->


    <div class="card-footer">

        <a href="/atividades/novaatividade" class="btn btn-lg btn-primary" role="button" style="margin-top: 30px ">Adicionar Atividade</a>
        <a href="/atividades/atividadesconcluidas" class="btn btn-lg btn-success" role="button" style="margin-top: 30px ">Ver Atividades Concluídas</a>
</div>


@endsection








