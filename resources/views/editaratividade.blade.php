@extends('layout.app', ["current" => "atividades"])

@section('body')

<div class="card border">
    <h4 style="margin: 20px">Editar Atividade</h4>
    <div class="card-body">
        <form action="/atividades/{{$atividade->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="tipo">Tipo de Atividade</label>
                <input type="text" class="form-control {{ $errors->has('tipo') ? 'is-invalid' : ''}}" name="tipo" id="tipo" value="{{$atividade->tipo}}" placeholder="Tipo de Atividade">
                @if($errors->has('tipo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tipo')}}
                    </div>
                @endif
            </div>
            
             <div class="form-group">
                <label for="dataentrega">Data de Entrega</label>
                <input type="date" class="form-control {{ $errors->has('tipo') ? 'is-invalid' : ''}}" name="dataentrega" id="dataentrega" value="{{$atividade->dataentrega->format('d/m/y')}}" >
                @if($errors->has('dataentrega'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tipo')}}
                    </div>
                @endif
            </div>

            <div class="form-group"> 
                            <label for="disciplina_id">Selecione a Disciplina</label>
            <select class="custom-select" name="disciplina_id" value="{{$atividade->disciplina_id}}">
                @foreach($disciplina as $dis)
                    @if($dis->id === $atividade->disciplina_id)
                        <option selected value="{{$dis->id}}">{{$dis->nome_disciplina}} - {{$dis->professor}}</option>
                    @else
                        <option value="{{$dis->id}}">{{$dis->nome_disciplina}} - {{$dis->professor}}</option>
                    @endif
                @endforeach
            </select>
            </div>
            
            <div class="form-group">
                <label for="descricao">Descrição da Atividade</label>
                <input type="text" class="form-control {{ $errors->has('tipo') ? 'is-invalid' : ''}}" name="descricao" id="descricao" value="{{$atividade->descricao}}" placeholder="Descrição Atividade">
                @if($errors->has('tipo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('descricao')}}
                    </div>
                @endif
            </div>
            

                
            <button type="submit" class="btn btn-primary btn-md" padding>Salvar</button>
            <!--<button type="cancel" class="btn btn-danger btn-sm">Cancel</button> -->
            <a href="/atividades" type="button " class="btn btn-md btn-danger">Cancelar</a>
        </form>
    </div>
</div>




@endsection