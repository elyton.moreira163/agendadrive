@extends('layout.app', ["current" => "disciplinas"])

@section('body')

<div class="card border">
    <h4 style="margin: 20px">Editar Disciplina</h4>
    <div class="card-body">
        <form action="/disciplinas/{{$disciplina->id}}" method="POST">
            @csrf
            <div class="form-group">
                <label for="nome_disciplina">Nome da Disciplina</label>
                <input type="text" class="form-control {{ $errors->has('nome_disciplina') ? 'is-invalid' : ''}}" name="nome_disciplina" 
                       id="nome_disciplina" value="{{$disciplina->nome_disciplina}}" placeholder="Disciplina">
               @if($errors->has('nome_disciplina'))
                    <div class="invalid-feedback">
                        {{ $errors->first('nome_disciplina')}}
                    </div>
                @endif   
            </div>
            <div class="form-group">
                <label for="professor">Nome do(a) Professor(a)</label>
                <input type="text" class="form-control {{ $errors->has('professor') ? 'is-invalid' : ''}}" name="professor" 
                       id="professor" value="{{$disciplina->professor}}" placeholder="Professor(a)">
                @if($errors->has('professor'))
                    <div class="invalid-feedback">
                        {{ $errors->first('professor')}}
                    </div>
                @endif
            </div>

            <!-- Domingo -->    
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(0))
                <input checked type="checkbox" class="custom-control-input" id="1" value="0">
            @else
                <input type="checkbox" class="custom-control-input" id="1" value="0">
            @endif
            <label class="custom-control-label" for="1">Domingo</label>
            <div class="form-group">
                <select class="form-control" name="tempo_domingo">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>    
                </select>
              </div>
            </div>
            <br>
                               
            <!-- Segunda-feira-->
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(1))
                <input checked type="checkbox" class="custom-control-input" id="2" value="1">
            @else
                <input type="checkbox" class="custom-control-input" id="2" value="1">
            @endif
            <label class="custom-control-label" for="2">Segunda-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_segunda">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <!-- Terça-feira -->
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(2))
                <input checked type="checkbox" class="custom-control-input" id="3" value="2">
            @else
                <input type="checkbox" class="custom-control-input" id="3" value="2">
            @endif
            <label class="custom-control-label" for="3">Terça-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_terca">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <!-- Quarta-feira -->
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(3))
                <input checked type="checkbox" class="custom-control-input" id="4" value="3">
            @else
                <input type="checkbox" class="custom-control-input" id="4" value="3">
            @endif
            <label class="custom-control-label" for="4">Quarta-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_quarta">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <!-- Quinta-feira -->
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(4))
                <input checked type="checkbox" class="custom-control-input" id="5" value="4">
            @else
                <input type="checkbox" class="custom-control-input" id="5" value="4">
            @endif
            <label class="custom-control-label" for="5">Quinta-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_quinta">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <!-- Sexta-feira -->
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(5))
                <input checked type="checkbox" class="custom-control-input" id="6" value="5">
            @else
                <input type="checkbox" class="custom-control-input" id="6" value="5">
            @endif
            <label class="custom-control-label" for="6">Sexta-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_sexta">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <!-- Sabado -->
            <div class="custom-control custom-checkbox custom-control-inline">
            @if($disciplina->dias->contains(6))
                <input checked type="checkbox" class="custom-control-input" id="7" value="6">
            @else
                <input type="checkbox" class="custom-control-input" id="7" value="6" >
            @endif
            <label class="custom-control-label" for="7">Sábado</label>
            <div class="form-group">
                <select class="form-control" name="sabado">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <br>
            
            <button type="submit" class="btn btn-primary btn-md">Salvar</button>
            <!-- <button type="cancel" class="btn btn-danger btn-sm">Cancel</button> -->
            <a href="/disciplinas" type="button " class="btn btn-md btn-danger">Cancelar</a>
        </form>
    </div>
</div>

@endsection