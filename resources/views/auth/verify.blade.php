@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifique seu endereço de e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Um link de verificação está sendo enviado para o seu e-mail.') }}
                        </div>
                    @endif

                    {{ __('Antes de continuar, verifique seu e-mail.') }}
                    {{ __('Se você não recebeu o e-mail') }}, <a href="{{ route('verification.resend') }}">{{ __('Clique para receber novamente') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
