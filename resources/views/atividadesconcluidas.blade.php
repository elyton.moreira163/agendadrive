@extends('layout.app', ["current" => "atividades"])

@section('body')

 <div class="card border" style="margin-top: 20px">
    <div class="card-body">

 @if(count($atividadeConcluida) > 0)
        <div class="list-group" >
            <a class="list-group-item list-group-item-action flex-column align-items-start list-group-item-success">
                <div class="d-flex w-100 justify-content-between">
                    <h4 class="mb-1">Atividades Concluídas</h4>
                </div>
            </a>
        </div>
    @foreach($atividadeConcluida as $atiConcluida)


        <div class="list-group" style="margin-top: 30px">
            <a class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-1">Tipo de Atividade: {{$atiConcluida->tipo}}</h5>
                    <h5 class="mb-2">Data de entrega: {{$atiConcluida->dataentrega->format('d/m/Y')}}</h5>
                </div>
                <div class="d-flex w-100 justify-content-between">
                    @foreach($disciplina as $dis)
                        @if($dis->id ===  $atiConcluida->disciplina_id)
                            <h6 class="mb-1">Disciplina: {{$dis->nome_disciplina}}</h6>
                        @endif
                    @endforeach

                </div>
                <p class="mb-1">Descrição: {{$atiConcluida->descricao}}</p>
                <small>
                        <a href="/atividades/restaurar/{{$atiConcluida->id}}" class="btn btn-md btn-success" style="margin-right: 15px">Restaurar</a>
                        <a href="/atividades/apagarpermanente/{{$atiConcluida->id}}" class="btn btn-md btn-danger" style="margin-right: 15px">Apagar</a> 
                </small>

            </a>
        </div>
    @endforeach 

@else
        <div class="list-group" >
            <a class="list-group-item list-group-item-action flex-column align-items-start list-group-item-success">
                <div class="d-flex w-100 justify-content-between">
                    <h4 class="mb-1">Você não possui atividades concluídas</h4>
                </div>
            </a>
        </div>
@endif

     </div>
</div>




        <a href="/atividades" class="btn btn-lg btn-primary" role="button" style="margin-top: 30px">Voltar para Atividade</a>


@endsection
