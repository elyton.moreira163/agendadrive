@extends('layout.app', ["current" => "disciplinas"])

@section('body')

<div class="card border">
    <h4 style="margin: 20px">Nova Disciplina</h4>
    <div class="card-body">
        <form action="/disciplinas" method="POST">
            @csrf
            <div class="form-group">
                <label for="nome_disciplina">Nome da Disciplina</label>
                <input type="text" class="form-control {{ $errors->has('nome_disciplina') ? 'is-invalid' : ''}}" name="nome_disciplina" 
                       id="nome_disciplina" placeholder="Disciplina"  value="{{ old('nome_disciplina') }}">
                @if($errors->has('nome_disciplina'))
                    <div class="invalid-feedback">
                        {{ $errors->first('nome_disciplina')}}
                    </div>
                @endif               
            </div>
            <div class="form-group">
                <label for="professor">Nome do(a) Professor(a)</label>
                <input type="text" class="form-control {{ $errors->has('professor') ? 'is-invalid' : ''}}" name="professor" 
                       id="professor" placeholder="Professor(a)" value="{{ old('professor') }}">
                @if($errors->has('professor'))
                    <div class="invalid-feedback">
                        {{ $errors->first('professor')}}
                    </div>
                @endif
            </div>
            <!--
            <input type="checkbox" name="diasemana[]" value="segunda">segunda
            <input type="checkbox" name="diasemana[]" value="segunda">segunda
            <input type="checkbox" name="diasemana[]" value="segunda">segunda
            <input type="checkbox" name="diasemana[]" value="segunda">segunda<br> 
            -->
  
            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="domingo" value="0" name="dias[]">
            <label class="custom-control-label" for="domingo">Domingo</label>
            <div class="form-group">
                <select class="form-control" name="tempo_domingo">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>    
                </select>
              </div>
            </div>
            <br>
            
            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="segunda" value="1" name="dias[]">
            <label class="custom-control-label" for="segunda">Segunda-feira    </label>
            <div class="form-group">
                <select class="form-control" name="tempo_segunda">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="terca" value="2" name="dias[]">
            <label class="custom-control-label" for="terca">Terça-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_terca">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>                
                
            </div>
            <br>
            
            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="quarta" value="3" name="dias[]">
            <label class="custom-control-label" for="quarta">Quarta-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_quarta">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>    
                
            </div>
            <br>

            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="quinta" value="4" name="dias[]">
            <label class="custom-control-label" for="quinta">Quinta-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_quinta">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>

            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="sexta" value="5" name="dias[]">
            <label class="custom-control-label" for="sexta">Sexta-feira</label>
            <div class="form-group">
                <select class="form-control" name="tempo_sexta">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            <br>
            
            <div class="custom-control custom-checkbox custom-control-inline">
            <input type="checkbox" class="custom-control-input" id="sabado" value="6" name="dias[]">
            <label class="custom-control-label" for="sabado">Sábado</label>
           <div class="form-group">
                <select class="form-control" name="sabado">   
                    <option selected="selected" disabled="disabled">Selecione o Tempo</option>
                    <option value="1">  1º e 2º Tempos</option>  
                    <option value="2">  3º e 4º Tempos</option>  
                </select>
            </div>
            </div>
            

            <br>
            
            <button type="submit" class="btn btn-primary btn-md" >Salvar</button>
            <!-- <button type="cancel" class="btn btn-danger btn-sm">Cancel</button> -->
            <a href="/disciplinas" type="button " class="btn btn-md btn-danger">Cancelar</a>
        </form>
    </div>
</div>




@endsection