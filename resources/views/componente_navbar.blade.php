<nav class="navbar navbar-expand-xl navbar-dark bg-primary rounded">
  <button class="navbar-toggler" type="button" data-toggle="collapse" 
          data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
    
<a class="navbar-brand" href="/">AgenDrive</a>
  <div class="collapse navbar-collapse" id="navbar">
    <ul class="navbar-nav mr-auto ">
      <li @if($current=="home") class="nav-item active" @else class="nav-item" @endif>
        <a class="nav-link" href="/">Home </a>
      </li>
      <li @if($current=="disciplinas") class="nav-item active" @else class="nav-item" @endif>
        <a class="nav-link" href="/disciplinas">Disciplinas</a>
      </li>
      <li @if($current=="atividades") class="nav-item active" @else class="nav-item" @endif>
        <a class="nav-link" href="/atividades">Atividades </a>
      </li>
      <li @if($current=="sobre") class="nav-item active" @else class="nav-item" @endif>
        <a class="nav-link" href="/sobre">Sobre </a>
      </li>      
    
        <li>
        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> 
            {{ __('Sair') }}
        </a>
            
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
            </form>
            
      </li>
        
    </ul>

  </div>
</nav>