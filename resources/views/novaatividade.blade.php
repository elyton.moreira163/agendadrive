@extends('layout.app', ["current" => "atividades"])

@section('body')

<div class="card border">
    <h4 style="margin: 20px">Nova Atividade</h4>
    <div class="card-body">
        <form action="/atividades" method="POST">
            @csrf
            <div class="form-group">
                <label for="tipo">Tipo de Atividade</label>
                <input type="text" class="form-control {{ $errors->has('tipo') ? 'is-invalid' : ''}}" name="tipo" id="tipo" placeholder="Tipo de Atividade" value="{{ old('tipo') }}">
                @if($errors->has('tipo'))
                    <div class="invalid-feedback">
                        {{ $errors->first('tipo')}}
                    </div>
                @endif
            </div>
            
             <div class="form-group">
                <label for="dataentrega">Data de Entrega</label>
                <input type="date" class="form-control {{ $errors->has('dataentrega') ? 'is-invalid' : ''}}" name="dataentrega" id="dataentrega" value="{{ old('dataentrega') }}">
                @if($errors->has('dataentrega'))
                    <div class="invalid-feedback">
                        {{ $errors->first('dataentrega')}}
                    </div>
                @endif
            </div>
            
            <div class="form-group">
                <label for="disciplina_id">Selecione a Disciplina</label>
                <select class="form-control {{ $errors->has('disciplina_id') ? 'is-invalid' : ''}}" id="disciplina_id" name="disciplina_id">
                  <option selected="selected" disabled="disabled">Selecione a Disciplina</option>
                @foreach($disciplina as $dis)
                  <option value="{{$dis->id}}">{{$dis->nome_disciplina}} - {{$dis->professor}}</option>
                @endforeach
                                                  @if($errors->has('disciplina_id'))
                    <div class="invalid-feedback">
                        {{ $errors->first('disciplina_id')}}
                    </div>
                @endif
                </select>
  
              </div>

            <div class="form-group">
                <label for="descricao">Descrição da Atividade</label>
                <input type="text" class="form-control {{ $errors->has('descricao') ? 'is-invalid' : ''}}" name="descricao" id="descricao" placeholder="Descrição Atividade" value="{{ old('descricao') }}">
               <!-- <textarea class="form-control {{ $errors->has('descricao') ? 'is-invalid' : ''}}" id="descricao" rows="3" placeholder="Descrição" value="{{ old('descricao') }}" ></textarea> -->
                @if($errors->has('descricao'))
                    <div class="invalid-feedback">
                        {{ $errors->first('descricao')}}
                    </div>
                @endif
            </div>
            

            <button type="submit" class="btn btn-primary btn-md" >Salvar</button>
            <!--<button type="cancel" class="btn btn-danger btn-sm">Cancel</button> -->
            <a href="/atividades" type="button " class="btn btn-md btn-danger">Cancelar</a>
        </form>
    </div>
</div>




@endsection

