@extends('layout.app', ["current" => "disciplinas"])

@section('body')


 <div class="card border">
    <div class="card-body">
        <h5 class="card-title">Suas Disciplinas</h5>

@if(count($dis) > 0)
        <table class="table table-ordered table-hover table-responsive-xl">
            <thead class="thead-dark">
                <tr>
                   <!-- <th>Código</th> -->
                    <th>Nome da Disciplina</th>
                    <th>Professor(a)</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
    @foreach($dis as $d)
                <tr>
                  <!--  <td>{{$d->id}}</td> -->
                    <td>{{$d->nome_disciplina}}</td>
                    <td>{{$d->professor}}</td>
                    <td>
                        <a href="/disciplinas/editar/{{$d->id}}" class="btn btn-sm btn-primary">Editar</a>
                        <a href="/disciplinas/apagar/{{$d->id}}" class="btn btn-sm btn-danger">Apagar</a>
                    </td>
                </tr>
    @endforeach                
            </tbody>
        </table>
@endif        
    </div>
    <div class="card-footer">
        <a href="/disciplinas/novadisciplina" class="btn btn-lg btn-primary" role="button" style="margin-right: 20px">Adicionar Disciplina</a>
        
        <a href="/restaurardisciplina" class="btn btn-lg btn-success" role="button">Ver Disciplinas Apagadas</a>
        
    </div>
</div>







@endsection