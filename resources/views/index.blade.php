@extends('layout.app', ["current" => "home"])

@section('body')

<div class="jumbotron bg-light border border-secondary">
    <div class="row">
        <div class="card-deck">
            <div class="card border border-primary">
                <div class="card-body">
                    <h5 class="card-title">Acesse suas Disciplinas</h5>
                    <p class="card=text">
                        Aqui você gerencia suas discplinas e seus professores do semestre.
                    </p>
                    <a href="/disciplinas" class="btn btn-primary">Ir para Disciplinas</a>
                </div>
            </div>
            <div class="card border border-primary">
                <div class="card-body">
                    <h5 class="card-title">Acesse suas Atividades</h5>
                    <p class="card=text">
                        Aqui você gerencia suas atividades referentes às suas disciplinas.                                 
                    </p>
                    <a href="/atividades" class="btn btn-primary">Ir para Atividades</a>
                </div>
            </div>            
        </div>
    </div>
    
    <h3 style="margin-top: 30px">Seus Horários</h3>

        <!--
    <table class="table table-bordered table-striped table-responsive-md table-condensed"  style="margin-top: 30px" >
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Domingo</th>
      <th scope="col">Segunda-feira</th>
      <th scope="col">Terça-feira</th>
      <th scope="col">Quarta-feira</th>
      <th scope="col">Quinta-feira</th>
        
      <th scope="col">Sexta-feira</th>
      <th scope="col">Sábado</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1º Tempo</th>
      <td></td>
      <td>Direito e Ética Profissional Aplicada a Computação</td>
      <td>Psicologia Organizacional</td>
      <td>Trabalho de Conclusão de Curso II</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">2º Tempo</th>
      <td></td>
      <td>Direito e Ética Profissional Aplicada a Computação</td>      
      <td>Psicologia Organizacional</td>
      <td>Trabalho de Conclusão de Curso II</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">3º Tempo</th>
      <td></td>
      <td>Psicologia Organizacional</td>
      <td> </td>
      <td>Estágio Supervisionado</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">4º Tempo</th>
      <td></td>
      <td>Psicologia Organizacional</td>
      <td> </td>
      <td>Estágio Supervisionado</td>
      <td></td>
      <td></td>
      <td></td>
    </tr>

  </tbody>
</table>
</div> -->
    
    <table class="table table-bordered table-striped table-responsive-md table-condensed"  style="margin-top: 30px" >
  <thead>
    <tr>
      <th scope="col"></th>
      <th scope="col">Domingo</th>
      <th scope="col">Segunda-feira</th>
      <th scope="col">Terça-feira</th>
      <th scope="col">Quarta-feira</th>
      <th scope="col">Quinta-feira</th>
        
      <th scope="col">Sexta-feira</th>
      <th scope="col">Sábado</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1º Tempo</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">2º Tempo</th>
      <td></td>
      <td></td>      
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">3º Tempo</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <th scope="row">4º Tempo</th>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>

  </tbody>
</table>

@endsection


   
