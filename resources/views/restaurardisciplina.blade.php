@extends('layout.app', ["current" => "disciplinas"])

@section('body')


 <div class="card border">
    <div class="card-body">
        <h5 class="card-title">Disciplinas Apagadas no Sistema</h5>

@if(count($disciplina) > 0)
        <table class="table table-ordered table-hover table-responsive-xl">
            <thead class="thead-dark">
                <tr>
                   <!-- <th>Código</th> -->
                    <th>Nome da Disciplina</th>
                    <th>Professor(a)</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
    @foreach($disciplina as $d)
                <tr>
                  <!--  <td>{{$d->id}}</td> -->
                    <td>{{$d->nome_disciplina}}</td>
                    <td>{{$d->professor}}</td>
                    <td>
                        <a href="/disciplinas/restaurar/{{$d->id}}" class="btn btn-md btn-success" style="margin-right: 15px">Restaurar</a>
                    </td>
                </tr>
    @endforeach                
            </tbody>
        </table>
@else
            <div class="card border" >
                <div class="card-body">
                    <p class="card=text">
                        Não possui disciplinas apagadas.                  
                    </p>   
                </div>
            </div>
@endif        
    </div>
    <div class="card-footer">
        <a href="/disciplinas" class="btn btn-lg btn-primary" role="button">Voltar para Disciplinas</a>
    </div>
</div>





@endsection