<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Disciplina;
use App\DiaDisciplina;

use Illuminate\Support\Facades\Auth;

class ControladorDisciplina extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dis = Disciplina::all()->where('user_id', Auth::user()->id);
        return view ('disciplinas', compact('dis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('novadisciplina');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
       $regras = [
          'nome_disciplina'=> 'required',  
          'professor'=> 'required'  
        ];
        $mensagens =[
          'nome_disciplina.required' => 'Digite o nome da Disciplina.',  
          'professor.required' => 'Digite o nome do Professor.'  
        ];
        
        $request->validate($regras, $mensagens);

        $disciplina = new Disciplina();
        $disciplina->nome_disciplina = $request->input('nome_disciplina');
        $disciplina->professor = $request->input('professor');
        $disciplina->user_id = Auth::user()->id;
        $disciplina->save();
        
        $dias = $request->input('dias');
        if (isset($dias)) {
            foreach($dias as $dia) {
                $diaDaDisciplina = new DiaDisciplina();
                switch ($dia) {
                    case 0:
                        $tempo = $request->input('tempo_domingo');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                    case 1:
                        $tempo = $request->input('tempo_segunda');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                    case 2:
                        $tempo = $request->input('tempo_terca');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                    case 3:
                        $tempo = $request->input('tempo_quarta');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                    case 4:
                        $tempo = $request->input('tempo_quinta');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                    case 5:
                        $tempo = $request->input('tempo_sexta');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                    case 6:
                        $tempo = $request->input('tempo_sabado');
                        $diaDaDisciplina->tempo = $tempo;
                        break;
                }
                
                $diaDaDisciplina->disciplina_id = $disciplina->id;
                $diaDaDisciplina->dia = $dia;
                $diaDaDisciplina->save();
            }
        }
        
        /*
        if (isset($request->input('tempo_segunda'))) {
            
        } 
        
        if (isset($request->input('tempo_terca'))) {
            
        } 
        
        if (isset($request->input('tempo_quarta'))) {
            
        } 
        
        if (isset($request->input('tempo_quinta'))) {
            
        } 
        
        if (isset($request->input('tempo_sexta'))) {
            
        } 
        
        if (isset($request->input('tempo_sabado'))) {
            
        } */
        
        return redirect('/disciplinas');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function ordenarDisciplinas() 
    {
        $dis = Disciplina::all()->where('user_id', Auth::user()->id);
        return view('index', compact('dis'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $disciplina = Auth::user()->disciplinas()->where('id', $id)->first();
        
        if(isset($disciplina)){
            $disciplina->dias = DiaDisciplina::all()->where('disciplina_id', $disciplina->id)->pluck('dia');
            return view('editardisciplina', compact('disciplina'));
        }
        
        return redirect ('/disciplinas');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $regras = [
          'nome_disciplina'=> 'required',  
          'professor'=> 'required'  
        ];
        $mensagens =[
          'nome_disciplina.required' => 'Digite o nome da Disciplina.',  
          'professor.required' => 'Digite o nome do Professor.'  
        ];
        
        $request->validate($regras, $mensagens);
            
        $disciplina = Disciplina::find($id);
        
        if(asset($disciplina)){
            $disciplina->nome_disciplina = $request->input('nome_disciplina');
            $disciplina->professor = $request->input('professor');
            $disciplina->save();
        }
        
        
        
        return redirect ('/disciplinas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $disciplina = Disciplina::find($id);
        if(asset($disciplina)){
            $disciplina->delete();
        }
        return redirect('/disciplinas');
    }
    
    
    public function disciplina_apagada(){
        $disciplina = Disciplina::onlyTrashed()->get();
        return view('restaurardisciplina', compact('disciplina'));
        
    }
    
    public function restore($id){
        $disciplina = Disciplina::withTrashed()->find($id);
        if(isset($disciplina)){
            $disciplina->restore();
        }
        return redirect ('/restaurardisciplina');
    }
}
