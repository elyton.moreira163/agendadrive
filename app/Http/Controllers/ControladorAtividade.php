<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Atividade;
use App\Disciplina;
use App\Mail\Atividades;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

class ControladorAtividade extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $disciplina = Disciplina::withTrashed()->get();
        $atividade = Atividade::all()->where('user_id', Auth::user()->id);
        $atividadeConcluida = Atividade::onlyTrashed()->get();
        return view ('atividades', compact('atividade', 'atividadeConcluida', 'disciplina'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { 
        $disciplina = Disciplina::all()->where('user_id', Auth::user()->id);
        return view ('novaatividade', compact('disciplina'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $user = $request->user();// busca usuario logado 

        $regras = [
          'tipo'=> 'required',  
          'dataentrega'=> 'required',  
          'disciplina_id'=> 'required',  
          'descricao'=> 'required' 
        ];
        $mensagens =[
          'tipo.required' => 'Digite o nome de atividade.',  
          'dataentrega.required' => 'Insira a data de entrega da atividade.',
          'disciplina_id.required' => 'Escolha a disciplina da atividade.',
          'descricao.required' => 'Descreva a atividade.'
        ];
        
        $request->validate($regras, $mensagens);
        $atividade = new Atividade();
        $atividade->tipo = $request->input('tipo');
        $atividade->dataentrega = $request->input('dataentrega');
        $atividade->disciplina_id = $request->input('disciplina_id');
        $atividade->descricao = $request->input('descricao');
        $atividade->user_id = Auth::user()->id;
        
        
        //$atividade->user->disciplinas; // Faz busca dos dados da tabela relacionada
        //dd($atividade);
        
        $atividade->save();

        $nomeDisciplina = Disciplina::findOrFail($atividade->disciplina_id); // faz a busca da disciplina_id
        $atividade->nomeDisciplina = $nomeDisciplina->nome_disciplina; // encontrando a disciplina exibe o nome dela

        
        Mail::to($user, $atividade)->send(new Atividades($user, $atividade)); // configurações da classe mail, estando tudo ok email é enviado

        return redirect ('/atividades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $atividade = Auth::user()->atividades()->where('id', $id)->first();
        
        if(isset($atividade)){
            $disciplina = Disciplina::all();
            return view ('editaratividade', compact('atividade', 'disciplina'));
        }
        
        return redirect ('/atividades');   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $regras = [
          'tipo'=> 'required',  
          'dataentrega'=> 'required',  
          'disciplina_id'=> 'required',  
          'descricao'=> 'required' 
        ];
        $mensagens =[
          'tipo.required' => 'Digite o nome de atividade.',  
          'dataentrega.required' => 'Insira a data de entrega da atividade.',
          'disciplina_id.required' => 'Escolha a disciplina da atividade.',
          'descricao.required' => 'Descreva a atividade.'
        ];
        
        $request->validate($regras, $mensagens);

        $atividade = Atividade::find($id);
        if(isset($atividade)){
            $atividade->tipo = $request->input('tipo');
            $atividade->dataentrega = $request->input('dataentrega');      
            $atividade->disciplina_id = $request->input('disciplina_id');
            $atividade->descricao = $request->input('descricao');
            $atividade->save();
        }
        return redirect ('/atividades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $atividade = Atividade::find($id);
        if(isset($atividade)){
            $atividade->delete();
        }
        return redirect ('/atividades');
    }
    
    public function restore($id){
        $atividade = Atividade::withTrashed()->find($id);
        if(isset($atividade)){
            $atividade->restore();
        }
        return redirect ('/atividades');
    }
    
    public function apagarpermanente($id){
        $atividade = Atividade::withTrashed()->find($id);
        if(isset($atividade)){
            $atividade->forceDelete();
        }
        return redirect ('/atividades');
    }
    
    
    public function atividade_concluida(){
        $atividadeConcluida = Atividade::onlyTrashed()->get();
        $disciplina = Disciplina::withTrashed()->get();
        return view ('atividadesconcluidas', compact('atividadeConcluida', 'disciplina'));
    }
}
