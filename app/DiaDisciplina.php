<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DiaDisciplina extends Model
{
    public $timestamps = false;
}
