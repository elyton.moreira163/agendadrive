<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Atividades extends Mailable
{
    use Queueable, SerializesModels;
    public $user,$atividade;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$atividade)
    { 
        $this->user = $user;
        $this->atividade = $atividade;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return  $this->from('admin@admin.com')
                ->view('mails.novaAtividade')
                ->subject('Nova Atividade - AgenDrive')
                ->with('user', 'atividade', $this->user, $this->atividade);
    }
}
